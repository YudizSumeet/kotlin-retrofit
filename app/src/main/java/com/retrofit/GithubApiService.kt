package com.retrofit

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by yudizsolutionspvtltd on 24/07/17.
 */

interface GithubApiService {
    @GET("search/users")
    fun search(@Query("q") query: String): Observable<Result>


}